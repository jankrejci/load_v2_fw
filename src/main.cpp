/**  === Documentation section === **/

/**  === Includes section === **/
#include "main.h"

/**  === Functions section === **/

/**
   Initialization function
 **/
void setup()
{
    // disable stepper before power source is ready
    stepper.setEnablePin(ENABLE_PIN);
    // directionInvert, stepInvert, enableInvert
    stepper.setPinsInverted(INVERT_DIRECTION, false, true);
    stepper.disableOutputs();

    // initialize serial communication
    Serial.begin(115200);
    while (!Serial)
    {
        ;
    }

    pinMode(LCD_LIGHT, OUTPUT);
    digitalWrite(LCD_LIGHT, HIGH);

    tft.init();
    tft.setRotation(4);

    tft.fillScreen(TFT_BLACK);
    tft.setTextColor(TFT_WHITE, TFT_BLACK);
    tft.setTextSize(2);

    tft.setCursor(0, 0, 2);
    tft.print("Everybody dies");

    tft.setCursor(0, 30, 2);
    tft.print("WiFi: ");
    tft.print(default_wifi.ssid);

    tft.setCursor(0, 60, 2);
    tft.print("Pass: ");
    tft.println(default_wifi.pass);

    tft.setCursor(0, 300, 2);
    tft.println("Connecting");

    // wait for terminal to connect
    delay(1000);

    Serial.println();
    Serial.println("=== Join the dark side ===");
    listNetworks();
    // start wifi connection
    Serial.print("SET: Connecting to WiFi \"");
    Serial.print(default_wifi.ssid);
    Serial.println("\"");

#ifdef STATIC_IP_MODE
    WiFi.config(staticIP, subnet, gateway, dns);
    delay(100);
#endif
    WiFi.mode(WIFI_STA);
    WiFi.begin(default_wifi.ssid, default_wifi.pass);
    delay(100);
    WiFi.setHostname(HOSTNAME);
    delay(100);

    while (WiFi.status() != WL_CONNECTED)
    {
        // if (millis() > 15000)
        // {
        //     break;
        // }
        Serial.print(".");
        delay(1000);
    }
    Serial.println();

    if (WiFi.status() == WL_CONNECTED)
    {
        Serial.print("SET: Connected with IP ");
        Serial.println(WiFi.localIP());
        Serial.print("SET: Connected with hostname ");
        Serial.println(WiFi.getHostname());

        WiFi.setTxPower(WIFI_POWER_19_5dBm);

        wifiServer.begin();

        tft.setCursor(0, 300, 2);
        tft.println("Connected ");
    }
    else
    {
        Serial.println("ERR: WiFi not available");

        tft.setCursor(0, 300, 2);
        tft.println("NOT connected");
    }

    if (!MDNS.begin(HOSTNAME))
    {
        Serial.println("ERR: MDNS not available");
    }
    Serial.println("SET: mDNS responder started");

    // otaSetup();

    // local_ip
    String local_ip = WiFi.localIP().toString();
    char buffer[30];
    local_ip.toCharArray(buffer, 30);

    tft.setCursor(0, 90, 2);
    tft.print("IP: ");
    tft.print(buffer);

    tft.setCursor(0, 120, 2);
    tft.print("Host: ");
    tft.print(HOSTNAME);

    tft.setCursor(0, 150, 2);
    tft.print("RSSI: ");
    tft.print(WiFi.RSSI());
    tft.print(" dBm");

    tft.setCursor(0, 400, 2);
    tft.print("Load: ");
    tft.println("XXXX");

    // endstop button
    pinMode(BTN_PIN, INPUT_PULLUP);
    // hv trigger
    pinMode(TRIG_PIN, INPUT);
    // heartbeat
    pinMode(HEARTBEAT, OUTPUT);
    delay(10);

    // stepper setup
    setHomePosition();
    testRun();

    attachInterrupt(TRIG_PIN, trigEvent, RISING);

    Serial.println("SET: Setup done");
    printLoads();
}

/**
   Main loop
 **/
void loop()
{
    // wifi client connection
    if (!client)
    {
        noClient();
    }

    // wifi client message handling
    if (client.available())
    {
        socketEvent();
    }

    if (millis() - lcd_refresh_last > LCD_REFRESH_PERIOD)
    {
        lcdRefresh();
        lcd_refresh_last = millis();
    }

    ArduinoOTA.handle();
}

/**
   Handling function
   takes care for the event comes over wifi
 **/
void socketEvent()
{
    char input_char;
    char received_msg[MSG_MAX_LEN + 1];
    strcpy(received_msg, "");

    // overflow protection
    if (client.available() > MSG_MAX_LEN)
    {
        Serial.println("ERR: too long command");
        // client.println("ERR: too long command");
    }
    else
    {
        // read whole buffer in one loop
        while (client.available())
        {
            input_char = client.read();

            // correct ending of the string is '\n'
            if (input_char == '\n' || input_char == '\0')
            {
                processCommand(received_msg);
                break;
            }

            // skip invalid charracters
            if (input_char < 32 || input_char > 126)
            {
                continue;
            }
            appendChar(received_msg, input_char);
        }
    }
    // clean the buffer on exit
    while (client.available())
    {
        client.read();
    }
}

/**
 * Helper function
 * appends one charracter to the end of the string
 **/
void appendChar(char *string, char letter)
{
    int len = strlen(string);
    string[len] = letter;
    string[len + 1] = '\0';
}

/**
 * Handling function
 * processing commands   
 **/
void processCommand(char *message)
{
    stripStr(message, " \n\r");
    Serial.print("MSG: ");
    Serial.println(message);

    if (isNumber(message))
    {
        int load = atoi(message);
        goToLoad(load);
    }

    if (strcmp(message, "") == 0)
    {
        printHelp();
    }

    if (strcmp(message, "help") == 0)
    {
        printHelp();
    }

    if (strcmp(message, "home") == 0)
    {
        setHomePosition();
    }

    if (strncmp(message, "TRIG:", 5) == 0)
    {
        trigMove(message);
    }
}

/**
 * Helper function
 * checks if the string is number or not
**/
bool isNumber(char *string)
{
    int index = 0;
    while (string[index] != 0)
    {
        if (!isdigit(string[index]))
        {
            return (false);
        }
        index++;
        continue;
    }
    return (true);
}

/**
 * Helper function
 * strips the character from the begining and the end of the string
**/
void stripStr(char *string, const char *symbols)
{
    int count = strlen(symbols);
    int changed = 0;

    for (int i = 0; i < count; i++)
    {
        int last = strlen(string) - 1;
        if (string[last] == symbols[i])
        {
            string[last] = 0;
            changed = 1;
        }
        if (string[0] == symbols[i])
        {
            shiftStrLeft(string);
            changed = 1;
        }
    }
    if (changed)
    {
        stripStr(string, symbols);
    }
    return;
}

/**
 * Helper function
 * shifts string one character left
**/
void shiftStrLeft(char *string)
{
    int len = strlen(string);
    for (int i = 1; i < len; i++)
    {
        string[i - 1] = string[i];
    }
    string[len - 1] = 0;
}

/**
 * Helper function
 * returns position of the given load in dictionary like fashion
 * returns -1 if the given load is not found
 **/
int getPosition(int load)
{
    for (int i = 0; i < LOAD_COUNT; i++)
    {
        if (loads[i].resistance == load)
        {
            return (loads[i].position);
        }
    }
    return (-1);
}

/**
 * Helper function
 * returns load at the given position
 * returns -1 if position is not found
**/
int getLoad(int position)
{
    for (int i = 0; i < LOAD_COUNT; i++)
    {
        if (loads[i].position == position)
        {
            return (loads[i].resistance);
        }
    }
    return (-1);
}

/**
 * Helper function
 * returns index of the given load
**/
int getIndex(int position)
{
    for (int i = 0; i < LOAD_COUNT; i++)
    {
        if (loads[i].position == position)
        {
            return (i);
        }
    }
    return (-1);
}

/**
 * Setup function
 * initializes home position of the carriage
 **/
void setHomePosition()
{
    Serial.println("SET: Homing motors");
    stepper.setMaxSpeed(HOMING_SPEED);
    stepper.setAcceleration(HOMING_ACCEL);
    stepper.enableOutputs();

    // find the endstop fast and rough
    if (digitalRead(BTN_PIN) == 1)
    {
        stepper.setCurrentPosition(0);
        goToEndStop();
    }

    // move out from the endstop
    stepper.setCurrentPosition(0);
    stepper.moveTo(500);
    stepper.runToPosition();

    if (digitalRead(BTN_PIN) == 0)
    {
        Serial.println("ERR: Homing failed");
        return;
    }

    // find the precise position of the endstop
    stepper.setCurrentPosition(0);
    stepper.setMaxSpeed(FINE_SPEED);
    stepper.setAcceleration(FINE_ACCEL);
    goToEndStop();
    stepper.setCurrentPosition(0);

    // move to total end and set zero position
    stepper.moveTo(END_P);
    stepper.runToPosition();
    stepper.setCurrentPosition(0);

    // set working speed
    stepper.setMaxSpeed(TRAVEL_SPEED);
    stepper.setAcceleration(TRAVEL_ACCEL);

    stepper.disableOutputs();
    Serial.println("SET: Homing complete");
}

/**
 * Helper function
 * to test motor movement after initialisation
 **/
void testRun()
{
    stepper.enableOutputs();

    Serial.print("DBG: set load ");
    Serial.println(loads[LOAD_COUNT - 1].resistance);

    goToLoad(loads[LOAD_COUNT - 1].resistance);

    int run_time = millis();
    goToLoad(loads[0].resistance);
    run_time = millis() - run_time;

    stepper.disableOutputs();

    if (digitalRead(BTN_PIN) == 1)
    {
        Serial.println("ERR: Test run failed");
        return;
    }

    Serial.println("SET: Test run done");
    Serial.print("SET: run time ");
    Serial.print(run_time / 2);
    Serial.println(" ms");
}

/**
 * Helper function
 * moves carriage to the endstop position while homing
 **/
void goToEndStop()
{
    stepper.moveTo(-1 * MAX_TRAVEL);
    while (digitalRead(BTN_PIN) == 1)
    {
        stepper.run();
    }
}

/**
 * Handling function
 * setting target position
 **/
void goToLoad(int load)
{
    int position = getPosition(load);
    char buffer[MSG_MAX_LEN + 1];

    Serial.print("DBG: load ");
    Serial.println(load);

    if (load == 0 || position == -1)
    {
        sprintf(buffer, "ERR: wrong input");
        Serial.println(buffer);
        client.println(buffer);
        return;
    }

    // going to closest lower load
    int true_load = getLoad(position);
    if (load != true_load)
    {
        sprintf(buffer, "DBG: load not found, going to %i", true_load);
        Serial.println(buffer);
        // client.println(buffer);
    }

    // debug message
    sprintf(buffer, "MTR: load %i, pos %i", true_load, position);
    Serial.println(buffer);

    // locked while motor is running
    stepper.enableOutputs();
    stepper.moveTo(position);
    stepper.runToPosition();
    stepper.disableOutputs();

    actual_position = position;

    sprintf(buffer, "%d      ", true_load);

    tft.setCursor(0, 400, 2);
    tft.print("Load: ");
    tft.println(buffer);

    client.println(buffer);
    client.flush();
    client.stop();
}

/**
 * Handling function
 * connecting and disconnecting wifi clients
**/
void noClient()
{
    client.stop();
    if (client_connected)
    {
        client_connected = false;
        Serial.println("DBG: Clinet disconnected");
    }
    client = wifiServer.available();
    if (client)
    {
        client_connected = true;
        Serial.println("DBG: Clinet connected");
    }
    return;
}

/**
 * Helper function
 * prints all available loads
**/
void printLoads()
{
    Serial.print("SET: loads ");
    for (int i = 0; i < LOAD_COUNT; i++)
    {
        Serial.print(loads[i].resistance);
        Serial.print(", ");
    }
    Serial.println();
}

/**
 * Interface function
 * prints how to use this interface
**/
void printHelp()
{
    char msg[1000] = "";
    char buffer[100] = "";

    strcat(msg, "tady bude napoveda\n");
    Serial.println(msg);
    strcat(msg, "\n");
    Serial.println(msg);

    sprintf(buffer, "dostupne zateze:\n");
    for (int i = 0; i < LOAD_COUNT; i++)
    {
        sprintf(buffer, "%d, ", loads[i].resistance);
        strcat(msg, buffer);
    }
    strcat(msg, "\n");

    Serial.println(msg);
    client.print(msg);
    client.stop();
}

/**
 * Interface function
 * handles move after trigger
**/
void trigMove(char *message)
{
    char *split_msg;
    split_msg = strtok(message, ":");
    split_msg = strtok(NULL, ":");
    int load = atoi(split_msg);

    long start_time = millis();

    trigger_flag = 0;

    while (true)
    {
        if (millis() - start_time > TRIG_TIMEOUT)
        {
            Serial.println("ERR: Trig timeout");
            client.println("Trig timeout");
            client.stop();
            break;
        }
        if (trigger_flag == 1)
        {
            delay(10);
            goToLoad(load);
            break;
        }
    }
}

void otaSetup()
{
    // Port defaults to 3232
    ArduinoOTA.setPort(OTA_PORT);
    // Hostname defaults to esp3232-[MAC]
    ArduinoOTA.setHostname(HOSTNAME);
    ArduinoOTA
        .onStart([]()
                 {
                     String type;
                     if (ArduinoOTA.getCommand() == U_FLASH)
                         type = "sketch";
                     else // U_SPIFFS
                         type = "filesystem";

                     // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
                     Serial.println("Start updating " + type);
                 })
        .onEnd([]()
               { Serial.println("\nEnd"); })
        .onProgress([](unsigned int progress, unsigned int total)
                    { Serial.printf("Progress: %u%%\r", (progress / (total / 100))); })
        .onError([](ota_error_t error)
                 {
                     Serial.printf("Error[%u]: ", error);
                     if (error == OTA_AUTH_ERROR)
                         Serial.println("Auth Failed");
                     else if (error == OTA_BEGIN_ERROR)
                         Serial.println("Begin Failed");
                     else if (error == OTA_CONNECT_ERROR)
                         Serial.println("Connect Failed");
                     else if (error == OTA_RECEIVE_ERROR)
                         Serial.println("Receive Failed");
                     else if (error == OTA_END_ERROR)
                         Serial.println("End Failed");
                 });

    //ArduinoOTA.begin();
    Serial.println("SET: OTA started");
}

void lcdRefresh()
{
    int rssi;
    rssi = WiFi.RSSI();

    tft.setCursor(0, 150, 2);
    tft.print("                ");

    tft.setCursor(0, 150, 2);
    tft.print("RSSI: ");
    tft.print(WiFi.RSSI());
    tft.print(" dBm");
}

void trigEvent()
{
    trigger_flag = 1;
}

void listNetworks()
{
    // scan for nearby networks:
    Serial.println("** Scan Networks **");
    int numSsid = WiFi.scanNetworks();
    if (numSsid == -1)
    {
        Serial.println("Couldn't get a wifi connection");
        while (true)
            ;
    }

    // print the list of networks seen:
    Serial.print("number of available networks:");
    Serial.println(numSsid);

    // print the network number and name for each network found:
    for (int i = 0; i < numSsid; i++)
    {
        Serial.print(WiFi.SSID(i));
        Serial.print("\t");
        Serial.print(WiFi.RSSI(i));
        Serial.print(" dBm\n");
    }
}