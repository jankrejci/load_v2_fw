/**  === Documentation section === **/
/**
   Main program integrating all previous programs

   - starts when voltage is connected
   - homing of the carriage during setup phase
   - initializes serial communication
   - waiting for specifig string eg. "1000\n"
   - moves the carriage to specified position
**/

#ifndef MAIN_H
#define MAIN_H

/**  === Includes section === **/
#include <Arduino.h>
#include <AccelStepper.h>
#include <SPI.h>
#include <WiFi.h>
#include <ESPmDNS.h>
#include <ArduinoOTA.h>
#include <TFT_eSPI.h>

/**  === const section === **/

const int MSG_MAX_LEN = 100; // maximal length of the input string

const int LCD_RST = 23;
const int LCD_CE = 22;
const int LCD_DC = 21;
const int LCD_DIN = 19;
const int LCD_CLK = 18;

const int LCD_LIGHT = 16;

const int HEARTBEAT = 27;

const int BTN_PIN = 2; // endstop button pin

const int TRIG_PIN = 37;         // hv trigger pin
const long TRIG_TIMEOUT = 10000; // trigger waiting timeout

const int DIR_PIN = 26;    // stepper direction pin
const int STEP_PIN = 25;   // stepper step pin
const int ENABLE_PIN = 33; // stepper enable pin

const int IFACE_TYPE = 1; // stepper interface "driver"

// homing mode
// high accel to allow fast stop
const long HOMING_SPEED = 10000;
const long HOMING_ACCEL = 200000;

// fine homing mode
const long FINE_SPEED = 1000;
const long FINE_ACCEL = 10000;

// normal mode
// const long TRAVEL_SPEED = 500000;
// const long TRAVEL_ACCEL = 1000000;
const long TRAVEL_SPEED = 300000;
const long TRAVEL_ACCEL = 500000;

// const long TRAVEL_SPEED = 200000;
// const long TRAVEL_ACCEL = 300000;

// WiFi port
const int VISA_PORT = 5025;
const int OTA_PORT = 3232;

typedef struct wifi_login
{
    char ssid[50];
    char pass[50];
} wifi_login;

// wifi_login default_wifi = {"WiFi VNT", "vnttnv321"};
// wifi_login default_wifi = {"WiFi VNT2a", "vnttnv321"};
// wifi_login default_wifi = {"WiFi VNT3", "vnttnv321"};
// wifi_login default_wifi = {"VNTcko", "123@jdu*na#pivo"};
// wifi_login default_wifi = {"Vyvoj_pracovni", "DOGPES123"};
// wifi_login default_wifi = {"WiFiVNT", "VNTTNV321"};
// wifi_login default_wifi = {"zkusebna", "supermario"};
// wifi_login default_wifi = {"WIFIMX", "VNTTNV321"};
wifi_login default_wifi = {"Skynet", "Velkasvestka13"};

// wifi_login default_wifi = {"pdx_calib", "velkasvestka"};
const char *HOSTNAME = "load";

// const char* HOSTNAME = "lala";
// const char* HOSTNAME = "dipsy";
// const char* HOSTNAME = "winky";
// const char* HOSTNAME = "artur";

// const char* HOSTNAME = "ambroz";
// const char* HOSTNAME = "albert";
// const char* HOSTNAME = "augustyn";
// const char* HOSTNAME = "monitor";

// #define STATIC_IP_MODE
#ifdef STATIC_IP_MODE
const IPAddress staticIP(192, 168, 50, 101); //ESP static ip
const IPAddress gateway(192, 168, 50, 1);    //IP Address of your WiFi Router (Gateway)
const IPAddress subnet(255, 255, 255, 0);    //Subnet mask
const IPAddress dns(8, 8, 8, 8);             //DNS
#endif

/**  === Global variables section === **/
// WiFi server configuration
WiFiServer wifiServer(VISA_PORT);
WiFiClient client;
bool client_connected = false;

// LCD instance
TFT_eSPI tft = TFT_eSPI();

// dictionary like struct to define position of each resistor
struct load
{
    int resistance;
    int position;
};

// definitions of position for each resistor

// motor 200 steps / rev
// microstepping 32
// gear 20 teeth
// pitch 2 mm
// (200 * 32) / (20 * 2) = 160 steps / mm

const bool INVERT_DIRECTION = false;
const long END_P = 0;                          // total end position
const long MAX_TRAVEL = 25000;                 // maximum travel distance from home
const long INIT_P = 352;                       //
const long DELTA_P = 2240;                     // distance between loads
const int LOAD_COUNT = 12;                     // total count of resistors
const unsigned long LCD_REFRESH_PERIOD = 1000; // lcd refresh time

struct load loads[LOAD_COUNT] = {
    {9999, INIT_P}, // NC
    {6000, 1 * DELTA_P + INIT_P},
    {1000, 2 * DELTA_P + INIT_P},
    {680, 3 * DELTA_P + INIT_P},
    {500, 4 * DELTA_P + INIT_P},
    {400, 5 * DELTA_P + INIT_P},
    {180, 6 * DELTA_P + INIT_P},
    {91, 7 * DELTA_P + INIT_P},
    {40, 8 * DELTA_P + INIT_P},
    {5, 9 * DELTA_P + INIT_P},
    {9998, 10 * DELTA_P + INIT_P}, // NC
    {1, 11 * DELTA_P + INIT_P},    // yellow short
};

// instance of AccelStepper class to control stepper motor
AccelStepper stepper = AccelStepper(IFACE_TYPE, STEP_PIN, DIR_PIN);

// timer for motor movement
hw_timer_t *stepper_timer = NULL;
portMUX_TYPE stepper_timerMux = portMUX_INITIALIZER_UNLOCKED;

// state variable - actual load
int actual_position = -1;
unsigned long lcd_refresh_last = 0;
int trigger_flag = 0;
char received_msg[MSG_MAX_LEN + 1];

/**  === Function declarations section === **/
void setup();
void loop();
void socketEvent();
void appendChar(char *string, char letter);
void processCommand(char *message);
bool isNumber(char *string);
void stripStr(char *string, const char *symbols);
void shiftStrLeft(char *string);
int getPosition(int load);
int getLoad(int position);
int getIndex(int load);
void setHomePosition();
void testRun();
void goToEndStop();
void goToLoad(int load);
void noClient();
void printLoads();
void printHelp();
void trigMove(char *message);
void otaSetup();
void lcdRefresh();
void trigEvent();
void listNetworks();

#endif